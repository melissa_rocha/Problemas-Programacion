package practica.problemas;

/**
 *
 * Hacer un pseudocodigo que imprima los numeros del 1 al 100.
 *
 */

public class Problema1 {
    public static void main(String[] args) {
        for (int i = 1; i <= 100 ; i++) {
            System.out.println(i);
        }
    }
}
