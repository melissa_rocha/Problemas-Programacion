package practica.problemas;

import java.util.List;
import java.util.Arrays;

/**
 *
 * Crear un array unidimensional de 20 elementos con nombres de personas,
 * Visualizar los elementos de la lista debiendo ir cada uno en una fila distinta.
 *
 */

public class Problema33 {

    public static void main(String[] args){
        List<String> nombres = Arrays.asList("Juan", "Martin", "Sara", "Javier", "William", "Harry", "Gabriela", "Ale",
                "Melissa", "Luis", "Raul", "Ana", "Aaron", "Ximena", "Wendy", "Homero", "Julieta", "Fernando",
                "Samantha", "Jade");

        for(String nombre : nombres){
            System.out.println(String.format("Hola me llamo %s", nombre));
        }
    }
}
