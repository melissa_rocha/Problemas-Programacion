package practica.problemas;

/**
 *
 * Hacer un programa que imprima la suma de los 100 primeros numeros.
 *
 */

public class Problema4 {
    public static void main(String[] args){
        Integer suma = 0;
        for (int i = 0; i <= 100 ; i++) {
            suma += i;
        }
        System.out.println(String.format("El resultado de la suma de los primeros 100 numeros es: %s", suma));
    }
}