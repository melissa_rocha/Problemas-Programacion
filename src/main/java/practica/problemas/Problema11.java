package practica.problemas;

import java.util.Scanner;

/**
 *
 * Imprimir y contar los multiplos de 3 desde la unidad hasta un numero que
 * introducimos por teclado.
 *
 */

public class Problema11 {
    public static void main(String[] args){
        System.out.println("Introduzca un numero: ");
        Scanner in = new Scanner(System.in);
        String numero = in.nextLine();
        Integer aqui = Integer.valueOf(numero);
        Integer contador = 0;

        for (int i = 1; i <= aqui; i++) {
             if(i%3==0){
                 System.out.println(i);
                 contador ++;
             }
        }
        System.out.println(String.format("El total de numeros multiplos de 3 es: %s", contador));
    }
}
