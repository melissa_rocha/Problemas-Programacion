package practica.problemas;

import java.util.Scanner;

/**
 *
 * Hacer un pseudocodigo que imprima el mayor y el menor de una serie de
 * cinco numeros que vamos introduciendo por teclado.
 *
 */

public class Problema14 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        Integer num, max = 0, min = 0;

        for (int i = 1; i <= 5; i++) {
            System.out.print(i + " Ingrese un numero: ");
            num = in.nextInt();
            if (min != 0 && max != 0) {
                if (num > max) {
                    max = num;
                }
                if (num < min) {
                    min = num;
                }
            } else {
                min = num;
                max = num;
            }
        }

        System.out.println(String.format("El numero mayor es: %s", max));
        System.out.println(String.format("El numero menos es: %s", min));
    }
}
