package practica.problemas;

import java.util.Scanner;

/**
 *
 * Hacer un pseudocodigo que imprima todos los numeros naturales que hay
 * desde la unidad hasta un numero que introducimos por teclado.
 *
 */

public class Problema6 {
    public static void main(String[] args){
        System.out.println("Introduzca un numero: ");
        Scanner in = new Scanner(System.in);
        String numero = in.nextLine();
        Integer aqui = Integer.valueOf(numero);

        if (aqui > 0){
            for (int i = 1; i <= aqui; i++) {
                System.out.println(i);
            }
        }
    }
}