package practica.problemas;


import java.util.Scanner;

/**
 *
 * Introducir dos numeros por teclado. Imprimir los numeros naturales que
 * hay entre ambos numeros empezando por el más pequeño, contar cuantos hay y
 * cuantos de ellos son pares. Calcular la suma de los impares.
 *
 */

public class Problema15 {

    public static void main(String[] args){
        Scanner in = new Scanner(System.in);
        Integer n1 = 0;
        Integer n2 = 0;
        Integer contadorpar = 0;
        Integer contadorimp = 0;
        Integer contador = 0;

        System.out.println("Dame el primer numero: ");
        n1 = in.nextInt();

        System.out.println("Dame el segundo numero: ");
        n2 = in.nextInt();

        if(n1 < n2){
            for (int i = n1; i <= n2 ; i++) {
                System.out.println(i);

                if(i%2 == 0){
                    contadorpar ++;
                } else{
                    contadorimp ++;
                }

                contador ++;
            }
        }else{

            for (int i = n2; i <= n1 ; i++) {
                System.out.println(i);

                if(i%2 == 0){
                    contadorpar ++;
                } else{
                    contadorimp ++;
                }

                contador ++;

            }

        }

        System.out.println(String.format("El total de numeros es: %d", contador));

        System.out.println(String.format("El total de numeros pares es: %d", contadorpar));

        System.out.println(String.format("El total de numeros impares es: %d", contadorimp));

    }

}
