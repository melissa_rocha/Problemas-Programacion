package practica.problemas;

import java.util.Scanner;

/**
 *
 * Introducir un numero por teclado. Que nos diga si es positivo o negativo.
 *
 */

public class Problema9 {

    public static void main(String[] args){
        Scanner sc=new Scanner(System.in);
        System.out.println("Ingresa un numero: ");
        int a = sc.nextInt();

        if(a<0){
            System.out.println("El numero es negativo");
        }else{
            System.out.println("El numero es positivo");
        }
    }

}