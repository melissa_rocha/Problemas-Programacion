package practica.problemas;

/**
 *
 * Hacer un pseudocodigo que imprima los numeros del 1 al 100. Que calcule la
 * suma de todos los numeros pares por un lado, y por otro, la de todos los
 * impares.
 *
 */

public class Problema12 {
    public static void main(String[] ags){
        Integer par = 0;
        Integer impar = 0;

        for (int i = 1; i <=100 ; i++) {
            System.out.println(i);
            if(i%2 == 0){
                par += i;
            }else{
                impar += i;
            }
        }
        System.out.println(String.format("La suma de los numeros pares es: %s", par));
        System.out.println(String.format("La suma de los numeros impares es: %s", impar));
    }
}
