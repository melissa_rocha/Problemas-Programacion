package practica.problemas;

import java.util.Random;

/**
 * Simular el lanzamiento de una moneda al aire e imprimir si ha salido cara o
 * cruz.
 *
 */

public class Problema28 {

    public static void main(String[] args){

        Random rnd = new Random();
        if(rnd.nextInt(2) == 0){
            System.out.println("Es cara");
        }else{
            System.out.println("Es cruz");
        }
    }

}
