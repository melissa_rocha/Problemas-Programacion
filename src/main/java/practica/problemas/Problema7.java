package practica.problemas;

import java.awt.event.KeyEvent;
import java.util.Scanner;

/**
 *
 * Introducir tantas frases como queramos y contarlas.
 *
 */

public class Problema7 {

    public static void main(String[] args){
        Scanner in  = new Scanner(System.in);
        String frase = "";
        Integer contador = 0;

        System.out.println("Ingresa tantas frases quieras (Escribe la palabra terminar para salir del programa) ");

        while(!frase.equals("terminar")){
            frase = in.nextLine();
            contador ++;
        }

        System.out.println(String.format("El numero de frases ingresadas es %s", contador));
    }




}
