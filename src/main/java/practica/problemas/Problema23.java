package practica.problemas;

/**
 *
 * Hacer un pseudocodigo que imprima los números del 0 al 100, controlando las
 * filas y las columnas.
 *
 */

public class Problema23 {

    public static void main(String[] args){

        for (int i = 0; i <= 100 ; i++) {

            System.out.print(i);
            System.out.print("\t");

            if(i%10 == 0){
                System.out.print("\n");
            }

        }
    }

}
