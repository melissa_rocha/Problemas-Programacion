package practica.problemas;

/**
 *
 * Imprimir diez veces la serie de numeros del 1 al 10.
 *
 */

public class Problema16 {
    public static void main(String[] args){
        for (int x = 1; x <= 10 ; x++) {
            for (int i = 1; i <= 10 ; i++) {
                System.out.println(i);
            }
            System.out.println("----------------");
        }
    }
}

