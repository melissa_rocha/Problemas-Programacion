package practica.problemas;


import java.util.Scanner;

/**
 *
 * Hacer un pseudocodigo que solo nos permita introducir S o N.
 *
 */

public class Problema8 {

    public static void main(String[] args){

        Scanner in  = new Scanner(System.in);
        String letra = "";

        System.out.println("Ingresa la letra s o n para poder ingresar al programa: ");

        while(!letra.equals("s") && !letra.equals("n")){
            letra = in.nextLine();
        }

        System.out.println("Bienvenido al programa");
    }


}
