package practica.problemas;

/**
 *
 * Imprimir y contar los numeros que son multiplos de 2 o de 3 que hay entre
 * 1 y 100.
 *
 */

public class Problema13 {
    public static void main(String[] args){
        Integer multdos = 0;
        Integer multtres = 0;

        for (int i = 1; i <= 100 ; i++) {
            if(i%2 == 0){
                System.out.println("--------MULTIPLO DE DOS------");
                System.out.println(i);
                multdos ++;
            }else if (i%3 == 0){
                System.out.println("--------MULTIPLO DE TRES------");
                System.out.println(i);
                multtres ++;
            }

        }

        System.out.println(String.format("El total de numeros multiplos de dos es: %s", multdos));
        System.out.println(String.format("El total de numeros multiplos de tres es: %s", multtres));
    }
}
