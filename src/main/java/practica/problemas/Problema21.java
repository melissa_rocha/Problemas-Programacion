package practica.problemas;

/**
 *
 * Hacer un programa que calcule independientemente la suma de los pares y los
 * impares de los numeros entre 1 y 1000, utilizando un switch.
 *
 */

public class Problema21 {

    public static void main(String[] args){

        Integer pares = 0;
        Integer impares = 0;

        for (int i = 0; i <= 1000; i++) {
            switch(i%2){
                case 1: pares+=i;
                    break;
                default: impares +=i;

            }
        }
        System.out.println(String.format("La suma total de los pares del 1 al 1000 es: %s", pares));
        System.out.println(String.format("La suma total de los impares del 1 al 1000 es: %s", impares));

    }
}
