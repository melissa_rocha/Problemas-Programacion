package practica.problemas;

import java.util.Scanner;


/**
 *
 * Calcular el factorial de un numero, mediante subprogramas.
 *
 */

public class Problema20 {

    public static void main(String[] args){
        Scanner in = new Scanner(System.in);

        System.out.println("Introduzca un número");
        Integer n = in.nextInt();

        if(n == 0){
            System.out.println(String.format("El factorias del número es: %s", 1));
        } else {
            System.out.println(String.format("El factorial de numero %s es: %s", n, factorial(n)));
        }
    }

    public static long factorial(long number) {
        if (number <= 1)
            return 1;
        else
            return number * factorial(number - 1);
    }
}
