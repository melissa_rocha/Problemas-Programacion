package practica.problemas;

import java.util.Random;

/**
 * Simular cien tiradas de dos dados y contar las veces que entre los dos suman 10.
 *
 */

public class Problema29 {

    public static void main(String[] args){

        Random rdm = new Random();
        Integer contar = 0;


        for (int i = 0; i < 100; i++) {

            Integer d1 = 0;
            Integer d2 = 0;

            while(d1==0){
                d1 = rdm.nextInt(7);
            }
            while(d2==0){
                d2 = rdm.nextInt(7);
            }

            if(d1 + d2 == 10){
                contar++;
            }

        }

        System.out.println(String.format("En total de veces que sumaron 10 los dados fue: %s", contar));
    }

}
