package practica.problemas;

/**
 *
 * Hacer un pseudocodigo que imprima los numeros pares entre 0 y 100.
 *
 */

public class Problema3 {
    public static void main(String[] args){
        for (int i = 0; i <= 100 ; i++) {
            if(i%2 == 0){
                System.out.println(i);
            }
        }
    }
}

