package practica.problemas;

import java.util.Scanner;

/**
 *
 * Hacer un programa que nos permita introducir un numero por teclado y sobre el
 * se realicen las siguientes operaciones: comprobar si es primo, hallar su
 * factorial o imprimir su tabla de multiplicar.
 *
 */

public class Problema32 {

    public static void main(String[] ags){
        Scanner in = new Scanner(System.in);
        System.out.print("Ingrese el numero con el que desea trabajar: ");
        Integer num = Integer.valueOf(in.nextInt());
        System.out.println("\t");

        System.out.println("Elija una opcion: ");
        System.out.println("1. Comprobar si es primo");
        System.out.println("2. Hallar su factorial");
        System.out.println("3. Imprimir su tabla de multiplciar");
        System.out.print("Opcion: ");

        Integer opc = Integer.valueOf(in.nextInt());

        switch(opc){
            case 1:
                System.out.print("Seleccionaste la opcion 1.");
                primo(num);
                break;
            case 2:
                System.out.println("Seleccionaste la opcion 2.");
                System.out.println(String.format("El factorial de %d es: %d", num, factorial(num)));
                break;
            case 3:
                System.out.println("Seleccionaste la opcion 3.");
                System.out.println(String.format("La tabla de multiplicar del numero %d es: ", num));
                tablaMultiplicar(num);
                break;

        }

    }

    public static void primo(Integer num){
        Integer a = 0, i;

        for(i=1;i<(num+1);i++){
            if(num%i==0){
                a++;
            }
        }
        if(a!=2){
            System.out.println("El numero no es primo");
        }else{
            System.out.println("El numero es primo");
        }

    }

    public static int factorial (Integer number) {

        if (number <= 1)
            return 1;
        else
            return number * factorial(number - 1);
    }

    public static void tablaMultiplicar(Integer n){

        if (n >= 0 && n <= 10){
            System.out.println("Tabla del " + n);
            for(int i = 1; i<=10; i++) {
                System.out.println(n + " * " + i + " = " + n * i);
            }
        }else{
            System.out.println("Ingrese un numero entre el 0 y 10");
        }
    }
}
