package practica.problemas;

/**
 *
 * Hacer un pseudocódigo que imprima los numeros impares hasta el 100 y que
 * imprima cuantos impares hay.
 *
 */

public class Problema5 {
    public static void main(String[] args){
        Integer contador = 0;
        for (int i = 0; i <=100 ; i++) {
            if(i%2 != 0){
                System.out.println(i);
                contador ++;
            }

        }
        System.out.println(String.format("El total de numeros impares es: %s", contador));
    }
}