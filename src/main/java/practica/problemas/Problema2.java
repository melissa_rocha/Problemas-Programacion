package practica.problemas;

/**
 *
 * Hacer un pseudocodigo que imprima los numeros del 100 al 0, en orden
 * decreciente.
 *
 */

public class Problema2 {

    public static void main(String[] args){
        for (int i = 100; i >= 0 ; i--) {
            System.out.println(i);
        }
    }
}