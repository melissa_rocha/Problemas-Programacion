package practica.problemas;

import java.util.Scanner;

/**
 *
 * Introducir una frase por teclado. Imprimirla cinco veces en filas
 * consecutivas, pero cada impresion ir desplazada cuatro columnas hacia la
 * derecha.
 *
 */

public class Problema22 {

    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        System.out.println("Dame una frase: ");
        String frase = sc.nextLine();

        for (int i = 0; i <5 ; i++) {

            System.out.print(String.format("%s    ", frase));

        }
    }
}
