package practica.problemas;

import com.sun.scenario.effect.impl.sw.sse.SSEBlend_SRC_OUTPeer;

import java.util.Scanner;

/**
 *
 * Introducir dos numeros por teclado y mediante un menu, calcule su suma, su
 * resta, su multiplicacion o su division.
 *
 */

public class Problema31 {

    public static void main(String[] args){
        Scanner in = new Scanner(System.in);
        System.out.println("Ingrese el primer numero");
        Integer n1 = Integer.valueOf(in.nextInt());
        System.out.println("\t");
        System.out.println("Ingrese el segundo numero");
        Integer n2 = Integer.valueOf(in.nextInt());

        System.out.println("Seleccione la opcion que desea realizar:");
        System.out.println("1. Suma");
        System.out.println("2. Resta");
        System.out.println("3. Multiplicacion");
        System.out.println("4. Division");
        System.out.print("Opcion: ");
        Integer opc = Integer.valueOf(in.nextInt());

        switch(opc){
            case 1:
                System.out.println(String.format("La suma de %s + %s = %s ", n1, n2, n1 + n2));
                break;
            case 2:
                System.out.println(String.format("La resta de %s - %s = %s", n1, n2, n1 - n2));
                break;
            case 3:
                System.out.println(String.format("La multiplicacion de %s * %s = %s", n1, n2, n1 * n2));
                break;
            case 4:
                if(n2 == 0){
                    System.out.println("No se puede dividir entre cero");
                }else{
                    System.out.println(String.format("La division de %s / %s = %s", n1, n2, n1 / n2));
                }
                break;
        }

    }


}
