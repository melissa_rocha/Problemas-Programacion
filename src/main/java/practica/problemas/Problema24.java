package practica.problemas;

import java.util.Scanner;

/**
 *
 * Comprobar si un numero mayor o igual que la unidad es primo.
 *
 */

public class Problema24 {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        Integer a=0,i,n;
        System.out.println("Ingrese numero");
        n=sc.nextInt();
        for(i=1;i<(n+1);i++){
            if(n%i==0){
                a++;
            }
        }
        if(a!=2){
            System.out.println("El numero no es primo");
        }else{
            System.out.println("El numero es primo");
        }
    }
}
