package practica.problemas;

import java.util.Scanner;

/**
 *
 * Introducir un numero por teclado. Que nos diga si es par o impar.
 *
 */

public class Problema10 {

    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        System.out.println("Ingresa un numero: ");
        int a = sc.nextInt();

        if(a%2==0){
            System.out.println(a + " es un numero par");
        }else{
            System.out.println(a + " es un numero impar");
        }
    }
}
