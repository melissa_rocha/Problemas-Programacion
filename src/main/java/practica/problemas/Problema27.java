package practica.problemas;

import java.util.Scanner;

/**
 *
 * Realizar la tabla de multiplicar de un numero entre 0 y 10.
 *
 */

public class Problema27 {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        Integer n;
        System.out.print("Introduce un número entero: ");
        n = sc.nextInt();
        if (n >= 0 && n <= 10){
            System.out.println("Tabla del " + n);
            for(int i = 1; i<=10; i++) {
                System.out.println(n + " * " + i + " = " + n * i);
            }
        }else{
            System.out.println("Ingrese un numero entre el 0 y 10");
        }

    }
}
