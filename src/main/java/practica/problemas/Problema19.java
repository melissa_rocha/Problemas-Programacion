package practica.problemas;

import java.util.Scanner;

/**
 *
 * Hacer un pseudocodigo que simule el funcionamiento de un reloj digital y
 * que permita ponerlo en hora.
 *
 */


public class Problema19 {

    public static void main (String[] args){
        Scanner in = new Scanner(System.in);
        Integer hora = 0;
        Integer minuto = 0;
        Integer segundo = 0;

        System.out.println("Dame hora, minuto y segundo");
        hora = in.nextInt();

        while(!(hora >= 0 && hora < 24)){
            System.out.println("La hora no cumple el estandar de las 24 horas");
            hora = in.nextInt();
        }

        minuto = in.nextInt();

        while(!(minuto >= 0 && minuto < 60)){
            System.out.println("Los minutos no cumplen el estandar");
            minuto = in.nextInt();
        }

        segundo = in.nextInt();

        while(!(segundo >= 0 && segundo < 60)){
            System.out.println("Los segundos no cumplen el estandar");
            segundo = in.nextInt();
        }

        System.out.println(String.format("Son las: %d:%d:%d", hora, minuto, segundo));
    }
}
